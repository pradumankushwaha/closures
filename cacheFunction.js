function cacheFunction(cb) {
     let cache=[];
     function invoke(n){
        if(cache[n]===undefined){
            cache[n]=cb(n);
            return cache[n];
        }else{
            let result=cb(n);
            cache[n]=result;
            return result;
        }
     }
     return invoke;
}
module.exports=cacheFunction;