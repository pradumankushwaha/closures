function limitFunctionCallCount(cb,n){
    let counter=0;
    function invoke(){
        counter++;
        if(counter<=n){
            return cb(7,8)
        }else{
            return null ;
        }
    }
    return invoke;
}
module.exports=limitFunctionCallCount;